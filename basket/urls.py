from django.urls import path
from basket import views


urlpatterns = [
    path('' , views.ult_cambios , name='Inicio'),
    path('teams/' , views.listar_equipos , name='Equipos'),
    path('players/' , views.listar_jugador , name='Jugadores'),
    path('coach/' , views.listar_entrenador , name='Entrenadores'),
    path('match/' , views.listar_partidos , name='Encuentros'),
]