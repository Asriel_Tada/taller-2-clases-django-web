from django.shortcuts import render
from django.utils import timezone
from .models import Coach, Team, Player, Roster,Match

# Create your views here.
def listar_jugador(request):
    template_name = 'jugador.html'
    jugs = Player.objects.all()
    equip = Team.objects.all()
    return render(request , template_name, {'jugs':jugs, 'equip':equip})

def listar_partidos(request):
    template_name = 'partidos.html'
    
    parts = Match.objects.all()
    return render(request , template_name, {'parts':parts})

def listar_entrenador(request):
    template_name = 'entrenador.html'
    
    entres = Coach.objects.all()
    return render(request , template_name,{'entres':entres})

def listar_equipos(request):
    template_name = 'equipos.html'

    equip = Team.objects.all()
    return render(request , template_name, {'equip':equip})

def ult_cambios(request):
    template_name = 'index.html'
    jugs = Player.objects.order_by('date')[:5:1]
    parts = Match.objects.reverse().order_by('date')[:5:-1]
    return render(request , template_name, {'parts':parts,'jugs':jugs} )